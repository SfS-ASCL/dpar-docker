This repository hosts Docker images of
[dpar](https://github.com/danieldk/dpar/) models.

## Available models

**Warning:** these models are provided for reproducibility. It is
*strongly* recommended to use [sticker2](https://github.com/stickeritis/sticker2/),
which outperforms dpar by a wide margin and jointly annotates
part-of-speech, morphology, dependencies, etc.

### German Hamburg-style dependencies

This model is similar to the model described in *Daniël de Kok and
Erhard Hinrichs, Transition-based dependency parsing with topological
fields, Proceedings of ACL 2016, Berlin, Germany*

The expected input format is CoNLL-X with the following layers:

* Tokens
* STTS part-of-speech tags in the `cpos` field
* The topological field as the `tf` feature in the `features` field.

You can use e.g. [sticker](https://gitlab.com/SfS-ASCL/dpar-docker/)
to obtain these annotations. This dpar model can then be run with:

```bash
$ docker run -it --rm \
  registry.gitlab.com/sfs-ascl/dpar-docker/de:20190325 \
  /bin/dpar-parse-de
```